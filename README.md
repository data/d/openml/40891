# OpenML dataset: AnomalyData_10percent_hd

https://www.openml.org/d/40891

**WARNING: This dataset is still in preparation.**

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Simulated data set. 1000 normal observations were drawn from a multivariate normal distribution of variable size 4, with mean zero and no correlation and variance for each feature between 1 and 10. About 100 observations (10%) are randomly sampled from numbers between 2 to 100 with replacement. The target variable is &quot;Target&quot;, with value &quot;Normal&quot; and &quot;Anomaly&quot;.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/40891) of an [OpenML dataset](https://www.openml.org/d/40891). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/40891/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/40891/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/40891/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

